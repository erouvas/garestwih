/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gar.wih;

import java.util.Objects;

public class JwtServiceResponse {

  private String idToken;
  private JwtServiceResult result;

  public JwtServiceResponse(String idToken, JwtServiceResult result) {
    this.idToken = idToken;
    this.result = result;
  }

  public JwtServiceResponse() {
  }

  public String getIdToken() {
    return idToken;
  }

  public void setIdToken(String idToken) {
    this.idToken = idToken;
  }

  public JwtServiceResult getResult() {
    return result;
  }

  public void setResult(JwtServiceResult result) {
    this.result = result;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 73 * hash + Objects.hashCode(this.idToken);
    hash = 73 * hash + Objects.hashCode(this.result);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final JwtServiceResponse other = (JwtServiceResponse) obj;
    if (!Objects.equals(this.idToken, other.idToken)) {
      return false;
    }
    if (!Objects.equals(this.result, other.result)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("JwtServiceResponse{idToken=").append(idToken);
    sb.append(", result=").append(result);
    sb.append('}');
    return sb.toString();
  }

}
