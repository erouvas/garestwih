package com.gar.wih;

import com.google.gson.Gson;
import org.jbpm.process.workitem.rest.RESTWorkItemHandler;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jbpm.process.workitem.rest.RESTServiceException;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GarJwtRestWIH extends RESTWorkItemHandler {

  private static final Logger logger = LoggerFactory.getLogger(GarJwtRestWIH.class);
  //
  private static final String JWT_SERVICE_URL = "jwt_service_url";
  private static final String JWT_SERVICE_DIALECT = "jwt_service_dialect";
  private static final String JWT_SERVICE_IP = "jwt_service_ip";
  private static final String JWT_SERVICE_CHANNEL = "jwt_service_channel";
  private static final String JWT_SERVICE_GUID = "jwt_service_guid";
  private static final String JWT_SERVICE_USERID = "jwt_service_userId";
  private static final String JWT_SERVICE_CLIENTID = "jwt_service_clientId";
  private static final String JWT_REFRESH_TOKEN = "refreshToken";
  //
  private ArrayList<String> optionKeys;
  private Map<String, String> optionValues;
  private Map<String, Map<String, String>> httpHeaders;

  /**
   * Obtain JWT token on WIH execution
   * https://github.com/kiegroup/jbpm/blob/master/jbpm-workitems/jbpm-workitems-rest/src/main/java/org/jbpm/process/workitem/rest/RESTWorkItemHandler.java#L752
   *
   * @param workItem
   * @param manager
   */
  @Override
  public void executeWorkItem(WorkItem workItem,
                              WorkItemManager manager) {

    boolean handleException = false;
    // extract required parameters
    String urlStr = (String) workItem.getParameter("Url");
    String method = (String) workItem.getParameter("Method");
    String handleExceptionStr = (String) workItem.getParameter("HandleResponseErrors");
    String resultClass = (String) workItem.getParameter("ResultClass");
    String acceptHeader = (String) workItem.getParameter("AcceptHeader");
    String acceptCharset = (String) workItem.getParameter("AcceptCharset");
    String headers = (String) workItem.getParameter(PARAM_HEADERS);

    if (urlStr == null) {
      throw new IllegalArgumentException("Url is a required parameter");
    }
    if (method == null || method.trim().length() == 0) {
      method = "GET";
    }
    if (handleExceptionStr != null) {
      handleException = Boolean.parseBoolean(handleExceptionStr);
    }
    Map<String, Object> params = workItem.getParameters();

    // authentication type from parameters
    // Overriding:
    // https://github.com/kiegroup/jbpm/blob/6fcc5bb9f354cbf32b1398f5d04aaa4dee2a1ae8/jbpm-workitems/jbpm-workitems-rest/src/main/java/org/jbpm/process/workitem/rest/RESTWorkItemHandler.java#L776
    // AuthenticationType authType = type;
    AuthenticationType authType = AuthenticationType.NONE;
    if (params.get(PARAM_AUTH_TYPE) != null) {
      authType = AuthenticationType.valueOf((String) params.get(PARAM_AUTH_TYPE));
    }

    // optional timeout config parameters, defaulted to 60 seconds
    Integer connectTimeout = getParamAsInt(params.get(PARAM_CONNECT_TIMEOUT));
    if (connectTimeout == null) {
      connectTimeout = 60000;
    }
    Integer readTimeout = getParamAsInt(params.get(PARAM_READ_TIMEOUT));
    if (readTimeout == null) {
      readTimeout = 60000;
    }
    if (headers == null) {
      headers = "";
    }

    //
    // - obtain JWT token and add it to headers
    //
    boolean goon = true;

    optionValues = new HashMap<>();
    optionKeys = new ArrayList<>();

    optionKeys.add(JWT_SERVICE_URL);
    optionKeys.add(JWT_SERVICE_DIALECT);
    optionKeys.add(JWT_SERVICE_IP);
    optionKeys.add(JWT_SERVICE_CHANNEL);
    optionKeys.add(JWT_SERVICE_GUID);
    optionKeys.add(JWT_SERVICE_USERID);
    optionKeys.add(JWT_SERVICE_CLIENTID);

    // - count the retry times
    int tryCount = 0;
    // - do the call until stopped explicilty
    boolean dotheCall = true;

    while (dotheCall) {
      //
      // populate HTTP headers with default values
      //
      httpHeaders = new HashMap<>();
      httpHeaders.put(JWT_SERVICE_CHANNEL, kv2m("channel", "0"));
      httpHeaders.put(JWT_SERVICE_CLIENTID, kv2m("clientId", "NO_VALUE"));
      httpHeaders.put(JWT_SERVICE_DIALECT, kv2m("dialect", "TR"));
      httpHeaders.put(JWT_SERVICE_GUID, kv2m("guid", "12345678-1234-1234-1234-123456789012"));

      {
        String default_ip = "NO_VALUE";
        try {
          default_ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
          // - log the exception, but continue nevertheless 
          java.util.logging.Logger.getLogger(GarJwtRestWIH.class.getName()).log(Level.SEVERE, null, ex);
        }
        httpHeaders.put(JWT_SERVICE_IP, kv2m("ip", default_ip));
      }

      httpHeaders.put(JWT_SERVICE_USERID, kv2m("userId", "0"));

      // - first time no need to refresh token
      httpHeaders.put(JWT_REFRESH_TOKEN, kv2m("refreshToken", (tryCount == 0 ? "false" : "true")));

      // - get values from system properties
      String configPrefix = this.getClass().getPackage().getName() + ".";
      optionKeys.stream().filter(key -> (this.isBlank(optionValues.get(key)))).forEachOrdered(key -> {
        optionValues.put(key, System.getProperty(configPrefix + key));
      });
      optionKeys.forEach(key -> {
        logger.info("GarJwtRestWIH - Config [" + key + "]=[" + optionValues.get(key) + "]");
      });

      try {

        String jwtToken = obtainJwtToken();
        goon = goon && (!this.isBlank(jwtToken));
        headers += ";token=" + jwtToken;

      } catch (IOException ex) {

        goon = false;
        logger.info("GarJwtRestWIH - ERROR JWT_TOKEN COULD NOT BE OBTAINED");
        ex.printStackTrace();
      }
      if (!goon) {
        throw new IllegalArgumentException("ERROR JWT_TOKEN COULD NOT BE OBTAINED");
      }

      HttpClient httpClient = getHttpClient(readTimeout,
                                            connectTimeout);

      Object methodObject = configureRequest(method,
                                             urlStr,
                                             params,
                                             acceptHeader,
                                             acceptCharset,
                                             headers);
      tryCount++;
      try {

        HttpResponse response = doRequestWithAuthorization(httpClient,
                                                           methodObject,
                                                           params,
                                                           authType);
        StatusLine statusLine = response.getStatusLine();
        int responseCode = statusLine.getStatusCode();
        Map<String, Object> results = new HashMap<String, Object>();
        HttpEntity respEntity = response.getEntity();
        String responseBody = null;
        String contentType = null;
        if (respEntity != null) {
          responseBody = EntityUtils.toString(respEntity, acceptCharset);

          if (respEntity.getContentType() != null) {
            contentType = respEntity.getContentType().getValue();
          }
        }

        // if response is 401, retry with new JWT token
        if (responseCode != 401) {

          if (responseCode >= 200 && responseCode < 300) {
            postProcessResult(responseBody, resultClass, contentType, results);
            results.put(PARAM_STATUS_MSG,
                        "request to endpoint " + urlStr + " successfully completed " + statusLine.getReasonPhrase());
          } else {
            if (handleException) {
              handleException(new RESTServiceException(responseCode, responseBody, urlStr));
            } else {
              logger.warn("Unsuccessful response from REST server (status: {}, endpoint: {}, response: {}",
                          responseCode,
                          urlStr,
                          responseBody);
              results.put(PARAM_STATUS_MSG, "endpoint " + urlStr + " could not be reached: " + responseBody);
            }
          }
          results.put(PARAM_STATUS, responseCode);

          // notify manager that work item has been completed
          manager.completeWorkItem(workItem.getId(), results);
          dotheCall = false;

        }
      } catch (Exception e) {
        handleException(e);
      } finally {
        try {
          close(httpClient, methodObject);
        } catch (Exception e) {
          handleException(e);
        }
      }

      dotheCall = dotheCall && (tryCount < 3);

    }//---
  }

  /**
   * Override any client caching, need to obtain JWT token on every call
   *
   * @return false
   */
  @Override
  public boolean getDoCacheClient() {
    return false;
  }

  private String obtainJwtToken() throws IOException {
    String jwt = "";

    if (this.isBlank(optionValues.get(JWT_SERVICE_URL))) {
      logger.info("GarJwtRestWIH - ERROR JWT_TOKEN SERVICE URL IS EMPTY :" + optionValues.get(JWT_SERVICE_URL));
      return "";
    }

    logger.info("GarJwtRestWIH - INVOKING GET_JWT_TOKEN " + optionValues.get(JWT_SERVICE_URL));

    RequestConfig config = RequestConfig.custom().setSocketTimeout(5000)
      .setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();

    HttpClientBuilder clientBuilder = HttpClientBuilder.create().setDefaultRequestConfig(config);
    clientBuilder.useSystemProperties(); // enable proxy

    HttpClient httpClient = clientBuilder.build();

    RequestBuilder builder = RequestBuilder.get().setUri(optionValues.get(JWT_SERVICE_URL));
    builder.addHeader(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
    builder.addHeader(HttpHeaders.ACCEPT, "application/json");
    builder.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");

    StringBuffer customHeaders = new StringBuffer("");

    httpHeaders.keySet().forEach(key -> {
      String val = this.isBlank(optionValues.get(key)) ? httpHeaders.get(key).get("value") : optionValues.get(key);
      customHeaders.append(httpHeaders.get(key).get("key")).append("=").append(val).append(";");
      logger.info("GarJwtRestWIH - Using [" + key + "]=[" + val + "]");
    });

    super.addCustomHeaders(customHeaders.toString(), builder::addHeader);

    HttpUriRequest request = builder.build();
    HttpResponse response = httpClient.execute(request);

    HttpEntity respEntity = response.getEntity();
    String responseBody = null;
    JwtServiceResponse jwtResponse = new JwtServiceResponse();

    // - assume all or nothing approach
    if (respEntity != null) {

      responseBody = EntityUtils.toString(respEntity, "UTF-8");

      Gson gson = new Gson();
      jwtResponse = gson.fromJson(responseBody, jwtResponse.getClass());

      if (jwtResponse != null && jwtResponse.getResult() != null) {
        if (jwtResponse.getResult().getReturnCode().equals("1")) {
          jwt = jwtResponse.getIdToken();
          logger.info("GarJwtRestWIH - JWT_TOKEN " + jwt);
        } else {
          logger.info("GarJwtRestWIH - JWT_TOKEN ERROR RESULT IS " + jwtResponse.getResult().getReturnCode() + " " + jwtResponse.getResult().getMessageText());
        }
      } else {
        logger.info("GarJwtRestWIH - JWT_TOKEN ERROR IS NULL");
      }

    }

    return jwt;
  }

  /**
   * convenience method
   *
   * @param str
   *
   * @return
   */
  public boolean isBlank(String str) {
    return (str == null) || (str.isEmpty()) || (str.length() == 0);
  }

  /**
   * convenience method to build a map out of two strings
   *
   * @param key
   * @param value
   *
   * @return
   */
  private HashMap<String, String> kv2m(String key, String value) {
    HashMap<String, String> kv = new HashMap<>();
    kv.put("key", key);
    kv.put("value", value);
    return kv;
  }

}
