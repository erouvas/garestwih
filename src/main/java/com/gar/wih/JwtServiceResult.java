/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gar.wih;

import java.util.Objects;

public class JwtServiceResult {
  
  private String guid;
  private String returnCode;
  private String messageText;

  public JwtServiceResult(String guid, String returnCode, String messageText) {
    this.guid = guid;
    this.returnCode = returnCode;
    this.messageText = messageText;
  }

  public JwtServiceResult() {
  }

  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  public String getReturnCode() {
    return returnCode;
  }

  public void setReturnCode(String returnCode) {
    this.returnCode = returnCode;
  }

  public String getMessageText() {
    return messageText;
  }

  public void setMessageText(String messageText) {
    this.messageText = messageText;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 89 * hash + Objects.hashCode(this.guid);
    hash = 89 * hash + Objects.hashCode(this.returnCode);
    hash = 89 * hash + Objects.hashCode(this.messageText);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final JwtServiceResult other = (JwtServiceResult) obj;
    if (!Objects.equals(this.guid, other.guid)) {
      return false;
    }
    if (!Objects.equals(this.returnCode, other.returnCode)) {
      return false;
    }
    if (!Objects.equals(this.messageText, other.messageText)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("JwtServiceResult{guid=").append(guid);
    sb.append(", returnCode=").append(returnCode);
    sb.append(", messageText=").append(messageText);
    sb.append('}');
    return sb.toString();
  }
  
  
}
